<div align="center">
    <h1>A Glorified Glorious Dotfiles</h1>
    <p>There's no place like <b><code>~</code></b> !</p>
    <p>Fork it. Steal it. I don't care. <b><code>Just don't make it ugly.</code></b></p>
</div>


## Table of Contents

- [Details](#starring)
- [Screenshots](#screenshots)
- [Dependencies](#dependencies)
    - [Required Dependencies](#required-dependencies)
    - [Optional Dependencies](#optional-dependencies)
- [Getting Started](#getting-started)
	- [Install](#install?)
- [Configuration](#configuration-and-preferences)
- [About Widgets and Modules](#about-widgets-and-modules)
- [TODOs](#todos)
- [Acknowledgement](#acknowledgement)



## Starring

- `archlinux` as distribution
- [`awesomewm`](https://awesomewm.org) as window manager
- `kitty` as terminal emulator
- [`tryone144's picom`](https://github.com/tryone144/compton/tree/feature/dual_kawase) as compositor
- [`rofi-git branch`](https://github.com/davatorium/rofihttps://github.com/davatorium/rofi) as application launcher
- `SFNS Display` as setup font
- [`trivago`](https://www.youtube.com/watch?v=oHg5SJYRHA0) as hotel


## Screenshots
**The screenshots are invisible right now.**



## Dependencies

### Required Dependencies
Dependencies needed to run the setup without errors

| Name | Description | Why/Where is it needed? |
| --- | --- | --- |
| [`awesome-git`](https://github.com/awesomeWM/awesome) |  Highly configurable framework window manager | isn't it obvious? |
| `rofi-git` | Window switcher, application launcher and dmenu replacement | Application launcher |
| `tryone144's picom` | A compositor for X11 | a compositor with kawase-blur |

### Optional Dependencies
Dependencies needed to achieve the setup's full potential. These are **optional**.

| Name | Description | Will be used by |
| --- | --- | --- |
| `xbacklight` | RandR-based backlight control application | Brightness widget and OSD |
| `alsa-utils` | An alternative implementation of Linux sound support | Volume widget and OSD |
| `acpi`,`acpid`,`acpi_call` | Show battery status and other ACPI info | Power/Battery Widgets. No need for this if you're not using a laptop |
| `mpd` | Server-side application for playing music | Music widget |
| `mpc` | Minimalist command line interface to MPD | Music widget |
| `maim` | Takes screenshots (improved `scrot`) | Screenshot keybinding |
| `feh` | Image viewer and wallpaper setter | Screenshot previews, wallpapers |
| `xclip` | Command line interface to the X11 clipboard | Will be used in saving the screenshots to clipboard |
| `xprop` | Property displayer for X | Custom titlebars for each client |
| `imagemagick` | An image viewing/manipulation program | Music widget/Extracts hardcoded album cover from songs |
| `blueman` | Manages bluetooth | default launch application for bluetooth widget |
| `xfce4-power-manager` | Manages power | default launch application for battery widget |
| `xdg_menu` or `awesome-freedesktop` | Generates a list of installed applications | Menu Module/Useful for generating app list |
| `noto-fonts-emoji` | Google Noto emoji fonts | Emoji support for notification center |




## Getting started

### Install?

1. Install the AwesomeWM **git master branch**. So if you're using the stable version or if you don't have AwesomeWM installed, install it now. Assuming you're using the best distro in the world - Arch Linux, you can install it by using an AUR helper. In this example let's use `yay`

	```bash
	yay -Syu awesome-git --removemake
	```

	Note that you can also compile it yourself. Clone the [repository](https://github.com/awesomeWM/awesome) then follow the instructions there to compile it.

2. Install the dependecies. You don't have to install it all, it's your choice after all. The only required packages are [these](#dependencies). You can change all the applications used in the setup with your preference.

3. If you have a current awesome configuration, make sure to create a backup in case of emergency.

4. Just copy the `awesome` folder to your `$HOME/.config/`.

5. Reload AwesomeWM by pressing `mod + shift + r`.




### Fix the font?

> **THE FONT IS TOO BIG!**

The setup uses `SFNS Display`. You need to install it or you can just use your preferred font family.




### Use the Powerleve10k ZSH theme?

1. Check the $SHELL you're using right now.

```bash
$ echo $SHELL
```

The output should be `/usr/bin/zsh`.

2. Not using ZSH? Well, it's a requirement. So to achieve the glory of Powerlevel10k you have to install it first. Assuming you're using the best distro in the universe, **Arch Linux**<sup>(i'm not joking)</sup>, you can install it by:

```bash
# Update your system then install zsh
$ pacman -Syu zsh
```

3. Change your $SHELL from whatever you're using right now to ZSH.

```bash
# User
$ chsh -s $(which zsh)

# System-wide
$ sudo chsh -s $(which zsh)
```

4. Now, you can install `oh-my-zsh` by:

```bash
# via curl
$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# via wget
$ sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

4. Download the recommended font [here](https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k). *Optional but highly recommended.*

5. Install the font by copying it to:

```bash
# User only
cp *.TTF $HOME/.local/share/fonts/TTF/

# System-wide
cp *.TTF /usr/share/fonts/TTF/
```
If the folder doesn't exist, create it.

7. Open your terminal. There should be some instructions/dialog there that will greet you and will guide you to theme your Powerlevel10k prompt.

8. More info about Powerlevel10k [here](https://github.com/romkatv/powerlevel10k). 




## Configuration and Preferences

+ **Configure theme's colors/aesthetic?**

	Awesome WM uses the `beautiful` library to beautify your setup.

	Change the values here:
	- `awesome/theme/default-theme.lua`
	- `awesome/theme/theme-name/init.lua`

+ **Configure Panels and bars?**
	
	The panels and sidebars are located in:
	- `awesome/layout/`
	- `awesome/widget/right-dashboard/`

	Top panel location:
	- `awesome/layout/top-panel.lua`

	Left panel location:
	- `awesome/layout/left-panel/init.lua`

	Right/Notificaton panel location:
	- `awesome/widget/right-dashboard/right-panel.lua`

	The right/notification panel is optional, you can remove it from the top panel.

+ **Configure Start-up and default applications**
	
	You can change the applications here:
	- `awesome/configuration/apps.lua`

+ **Configure Keybindings?**
	
	You can check keybinds by pressing `mod + F1`.

	Client keybindings:
	- `awesome/configuration/client/keys.lua`

	Global keybindings:
	- `awesome/configuration/keys/global.lua`

+ **Configure client rules?**
	
	The client rules manages the behaviour of the client. Is it floating? Is it above the other clients? Is it under the other clients, perhaps? What tag will the client spawn?

	Client rules:
	- `awesome/configuration/client/rules.lua`

+ **Configure client tags?**
	
	Tags are the "workspaces". Terminal, web browsers, text editors are few of the tags used here.

	Client tags:
	- `awesome/configuration/tags/init.lua`

+ **Configure the compositor?**
	
	The compositor we are using is tryone144's picom [feature/dual_kawase](https://github.com/tryone144/compton/tree/feature/dual_kawase) branch that provides the `kawase blur` shader. It gives the beautiful<sup>**beauty is subjective**</sup> blur effect.

	Compositor configuration file:
	-  `awesome/configuration/picom.conf`

+ **Configure rofi?**
	
	What is rofi? Rofi is a window switcher,  application launcher, ssh dialog and dmenu replacement

	Rofi location:
	- `awesome/configuration/rofi/`

	We will use two rofi configuration. One is for application launcher and the second one is for web searching.

	Rofi Application Launcher:
	- `awesome/configuration/rofi/appmenu/rofi.rasi`

	Rofi Web Search:
	- `awesome/configuration/rofi/sidebar/rofi.rasi`


+ **Weather and Email Credentials**
	
	You can put your credentials in the `awesome/secrets.lua` file.
	Notes:
	- Your credentials are exposed.
	- It is better to encrypt it by using GnuPG, for example.


## About Widgets and Modules

+ **Weather Widget**
	
	How to get a credentials for weather widget?

	- OpenWeatherMap is our weather provider. So go to OpenWeatherMap's [website](https://home.openweathermap.org/).
	- Register, log-in, and then go [here](https://home.openweathermap.org/api_keys) to generate your very own API keys. 
	- Put your credentials in `awesome/secrets.lua`.


+ **Email Widget**

	How to get a valid credentials for email widget?

	The widget uses an IMAP. 
	So it means that any email service provider that provides an IMAP support is supported by the widget.

	- You need an email_address.
	- You must generate an app password for your account. Your account password **WILL NOT WORK!** An App Password is required!
	- Just search the instrucion in the internet on how to generate an App Password for your email account.<sup>sorry i don't have an internet connection while typing this.</sup>>
		- For example `Create an app password for gmail account.`
	- You need an imap_server.
		- Just get your email service provider's imap server. Gmail's imap server is `imap.gmail.com`. You can search it in the internet.
	- Provide the port.
		Again, you can just search it in the internet. Gmail's port is `993`.


+ **Calculator Widget**
	
	The calculator widget is the result of my boredom. 
	- Supports:
		- Basic math operations
		- **KEYBOARD SUPPORT**

	- Tips:
		Enable keyboard support by hovering your mouse above the calculator.
		Or toggle it on/off by pressing the keyboard button.
		Only numbers, arithmetic operators, and decimal point is accepted.

	- Keyboard Binding:
		- `=` and `Return` to evaluate.
		- `BackSpace` to delete the last digit.
		- `Escape` to clear the screen.
		- `x` stops the keygrabbing.

	- Note:
		- While in keygrabbing mode, your keyboard's focus will be on the calculator. So you're AwesomeWM keybinding will stop working.<sup>temporarily of course.</sup> 

	- Stopping the keygrabbing mode:
		- Move away your cursor from the calculator.
		- Toggle it off using the keyboard button.
		- Press `x`.


+ **Dynamic Wallpaper Modu;e**
	
	Another fruit of my boredom. Yes, it changes the wallpaper based on the set time.

	- Note:
		- The wallpapers are in `awesome/theme/wallpapers/`
		- It has a four scheduled time:
			- `morning`, `noon`, `night`, `midnight`
		- Right now, it only search for `*.jpg` extension. You can change it by editing the wallpaper name in the `awesome/module/dynamic-wallpaper.lua`

+ **Music widget**

	- Depends:
		- `mpd`, `mpc`

	- Optional Depends:
		- music file with metadata
		- music file with hardcoded album cover

	It is better if you have a music file with metadata.


+ **Backdrop module**

	This module is developed by [PapyElGringo](https://github.com/PapyElGringo/) for his [material-awesome](https://github.com/PapyElGringo/material-awesome). This module adds a backdrop blur to the dialogs and modals. You can disable it by setting the `drawBackdrop` to `false` in the `awesome/configuration/client/rules.lua`



+ **Menu module**

	Yes, this is somewhat useless when we're using a window tiling managers, buuut... AwesomeWM is not a window tiling manager. It's a framework! And it handles floating clients pretty well. Sooo...

	- Depends:
		`xdg-menu` or [`awesome-freedesktop`](https://github.com/lcpz/awesome-freedesktop/). More info [here](https://wiki.archlinux.org/index.php/Awesome#Applications_menu)

	- `xdg-menu` needs a manual intervention.
	- `awesome-freedesktop` will populate the menu for you. More info [here](https://github.com/lcpz/awesome-freedesktop/)



## TODOs

- [x] Multi-monitor support  
- [ ] Scrollable Notification Center  
- [x] Adjust picom blur using a slider  
- [ ] Refactor for cleaner code  


## Acknowledgement
+ [**PapyElGringo**](https://github.com/PapyElGringo/) for the inspiration. I used his [marterial-awesome](https://github.com/PapyElGringo/material-awesome) as the kickstarter to start ricing again. `material-awesome` is one of the most beautiful and functional<sup>at the same time</sup> rice I have ever seen.
+ [**elenapan**](https://github.com/elenapan/dotfiles), [**addyfe**](https://github.com/addy-dclxvi/almighty-dotfiles) for the inspiration.
+ **pdonadeo** for the [Rofi Web Search](https://github.com/pdonadeo/rofi-web-search) script.
+ Also thanks to the and developers of AwesomeWM and to the contributors in my repo. Haha!