-- Printscreen/Screenshot bash script wrapped in lua and AwesomeWM API
-- Depends : maim, xclip

local awful = require('awful')
local naughty = require('naughty')

local ss = {}

-- Save location
local screenshot_dir = '$HOME/Pictures/Screenshots'


local check_save_location = function()
	check_dir_cmd = [[
		dir=]] .. screenshot_dir .. [[

		if [ ! -d $dir ]; then
		mkdir -p $dir
		fi
	]]

	awful.spawn.easy_async_with_shell(
		check_dir_cmd,
		function(stdout, stderr, out, reason) 
		end
	)
end


-- Check screenshot location at startup
-- Creates it if it doesn't exist
check_save_location()


local get_image_path = function()

	local get_path = [[
	dir=]] .. screenshot_dir .. [[

	echo $dir/`ls -1 -t $dir | head -1`
	]]

	awful.spawn.easy_async_with_shell(get_path, function(stdout) 
		-- Update image path
		image_path = stdout
	end)
end

-- The screenshot script
-- Lua inside a bash script and bash script inside lua?
-- As you can see it's a bit..overkill?
-- Well it has a purpose you uncultured swine!
-- Listen here you little shit.
-- Well, I'm too lazy to explain this and I already have comments.
-- So, just understand it you lil shit
local script_return = function(cmd, dir, msg)
	return [[
	dir=]] .. dir .. [[
		# Note: This is BASH or whatever shell you're using!

		# Save the file name
		# Useful when opening the screenshot image
		file_name=$dir/$(date +%Y%m%d_%H%M%S).png
		
		# The maim command
		]] .. cmd .. [[ $file_name

		# Copy to clipboard
		xclip -selection clipboard -t image/png -i $dir/`ls -1 -t $dir | head -1` &


		# A Lua/Awesome API Inside a bash script which is inside of Lua/Awesome API
		awesome-client "
		-- IMPORTANT NOTE: THIS PART OF THE SCRIPT IS LUA!
		naughty = require('naughty')
		awful = require('awful')

		local open_image = naughty.action {
			name = 'Open Image',
	    	icon_only = false,
		}

		local close = naughty.action {
			name = 'Close',
		    icon_only = false,
		}

		-- Execute the callback when `Open Image` is pressed
		open_image:connect_signal('invoked', function()
			awful.spawn('feh ' .. '$file_name')
		end)

		-- Show notification
		naughty.notification ({
			app_name = 'Screenshot Tool',
			icon = '$file_name',
			timeout = 60,
			title = 'Snap!',
			message = ]] .. msg .. [[,
			actions = { open_image, close }
		})

		"
	]]
end

local fullscreen_capture = function()
	
	-- Check again incase the user deletes it
	-- Yes, this is persistent biatch!
	check_save_location()

	local message = "'Screenshot saved and copied to clipboard!'"

	-- Generate a script
	local screenshot_script = script_return(
		'maim -u -m 1',
		screenshot_dir,
		message
	)

	-- Run the script
	awful.spawn.easy_async_with_shell(
		screenshot_script,
		function(stdout) 
			collectgarbage('collect')
		end
	)
	

end

local area_capture = function()
	
	-- Check again incase the user deletes it
	-- Yes, this is persistent biatch!
	check_save_location()

	local message = "'Screenshot saved and copied to clipboard!'"

	-- Generate a script
	local screenshot_script = script_return(
		'maim -u -s -m 1',
		screenshot_dir,
		message	
	)

	-- Run the script
	awful.spawn.easy_async_with_shell(
		screenshot_script,
		function(stdout) 
			collectgarbage('collect')
		end
	)

end

fullmode = function()
	fullscreen_capture()
end

areamode = function()
	area_capture()
end


ss.fullmode = fullmode
ss.areamode = areamode

return ss
