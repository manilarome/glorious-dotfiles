-- Toggle picom blur fx
--		depends : picom-tryone

local awful = require('awful')

local filesystem = require('gears.filesystem')
local config_dir = filesystem.get_configuration_dir()

local toggfx = {}

local toggle_fx = function(togglemode)
	local toggle_fx_script = [[
	picom_dir=$HOME/.config/awesome/configuration/picom.conf

	# Check picom if it's not running then start it
	if [ -z $(pgrep picom) ]; then
		picom -b --experimental-backends --dbus --config ]] .. config_dir .. [[/configuration/picom.conf
	fi

	case ]] .. togglemode .. [[ in
		'enable')
		sed -i -e 's/method = "none"/method = "dual_kawase"/g' "${picom_dir}"
		;;
		'disable')
		sed -i -e 's/method = "dual_kawase"/method = "none"/g' "${picom_dir}"
		;;
	esac
	]]

	-- Run the script
	awful.spawn.easy_async_with_shell(toggle_fx_script, function(stdout, stderr)

	end, false)

end


local enablefx = function()
	toggle_fx('enable')
end

local disablefx = function()
	toggle_fx('disable')
end

toggfx.enable = enablefx
toggfx.disable = disablefx

return toggfx
