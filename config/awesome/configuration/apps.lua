local filesystem = require('gears.filesystem')
local config_dir = filesystem.get_configuration_dir()

local get_dpi = require('beautiful').xresources.get_dpi


return {

	-- The default applications
	-- The applications are integrated in keybindings and widgets
	default = {
		terminal = 'kitty',                                                                                 -- Terminal Emulator
		text_editor = 'subl3',                                                                              -- GUI Text Editor
		web_browser = 'firefox',                                                                            -- Web browser
		file_manager = 'nemo',                                                                              -- GUI File manager
		lock = 'mantablockscreen -sc',                                                                      -- Lockscreen
		quake = 'kitty --name QuakeTerminal',                                                               -- Quake-like Terminal
		rofi = 'rofi -dpi ' .. get_dpi() .. ' -show Search -modi Search:' .. config_dir .. 
			'/configuration/rofi/sidebar/rofi-web-search.py' .. ' -theme ' .. config_dir .. 
				'/configuration/rofi/sidebar/rofi.rasi',                                                    -- Rofi Web Search
		rofiappmenu = 'rofi -dpi ' .. get_dpi() .. ' -show drun -theme ' .. config_dir .. 
			'/configuration/rofi/appmenu/rofi.rasi',                                                        -- Application Menu

		-- You can add more default applications here
	
	},
	
	-- List of apps to start once on start-up
	run_on_start_up = {
		'picom -b --experimental-backends --dbus --config ' .. config_dir .. '/configuration/picom.conf',   -- Compositor
		'blueman-applet',                                                                                   -- Bluetooth tray icon
		'xfce4-power-manager',                                                                              -- Power manager
		'/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &' .. 
			' eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)',                           -- Credential manager
		'xrdb $HOME/.Xresources',                                                                           -- Load X Colors
		'nm-applet',                                                                                        -- NetworkManager Applet
		'mpd',                                                                                              -- Music Server
		'pulseeffects --gapplication-service',                                                              -- Sound Equalizer
		'redshift-gtk -l 14.45:121.05',                                                                     -- Redshift
		'xidlehook --not-when-fullscreen --not-when-audio --timer 600 "mantablockscreen -sc" ""',           -- Auto lock timer

		-- You can add more start-up applications here
	},

	-- List of binaries and functions to execute a certain task
	bins = {
		full_screenshot = require('binaries.snap').fullmode,                                                -- Full Screenshot
		area_screenshot = require('binaries.snap').areamode,                                                -- Area Selected Screenshot
		selected_screenshot = require('binaries.snap').selectmode,                                          -- Slop Selected Screenshot
		enable_blur = require('binaries.togglewinfx').enable,                                               -- Enable Compositor Blur
		disable_blur = require('binaries.togglewinfx').disable,                                             -- Disable Compositor Blur
		extract_album = require('binaries.extractcover').extractalbum                                       -- Extract Album Cover From Media File
	}
}
