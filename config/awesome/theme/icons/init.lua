-- Icons directory
local dir = os.getenv('HOME') .. '/.config/awesome/theme/icons'

-- The tag list icon theme
--    Available Themes: 'material', 'macos', 'korla'
local taglist_icon_theme = 'korla'
local tit_dir = dir .. '/tag-list/tag/' .. taglist_icon_theme


return {

	-- Action Bar
	web_browser 		= tit_dir .. '/web-browser.svg',
	text_editor			= tit_dir .. '/text-editor.svg',
	social				= tit_dir .. '/social.svg',
	file_manager 		= tit_dir .. '/file-manager.svg',
	multimedia 			= tit_dir .. '/multimedia.svg',
	games 				= tit_dir .. '/games.svg',
	development 		= tit_dir .. '/development.svg',
	sandbox 			= tit_dir .. '/sandbox.svg',
	terminal 			= tit_dir .. '/terminal.svg',
	graphics 			= tit_dir .. '/graphics.svg',
	menu 				= tit_dir .. '/menu.svg',

	-- Others/System UI
	close 				= dir .. '/close.svg',
	logout				= dir .. '/logout.svg',
	sleep 				= dir .. '/power-sleep.svg',
	power 				= dir .. '/power.svg',
	lock 				= dir .. '/lock.svg',
	restart 			= dir .. '/restart.svg',
	search 				= dir .. '/magnify.svg',
	volume 				= dir .. '/volume-high.svg',
	brightness 			= dir .. '/brightness-7.svg',
	effects 			= dir .. '/effects.svg',
	chart 				= dir .. '/chart-areaspline.svg',
	memory 				= dir .. '/memory.svg',
	harddisk 			= dir .. '/harddisk.svg',
	thermometer 		= dir .. '/thermometer.svg',
	plus 				= dir .. '/plus.svg',
	batt_charging 		= dir .. '/battery-charge.svg',
	batt_discharging	= dir .. '/battery-discharge.svg',

}
