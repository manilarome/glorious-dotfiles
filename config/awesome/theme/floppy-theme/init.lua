local filesystem = require('gears.filesystem')
local theme_dir = filesystem.get_configuration_dir() .. '/theme'
local dpi = require('beautiful').xresources.apply_dpi

local theme = {}
theme.icons = theme_dir .. '/icons/'
theme.font = 'SFNS Display Regular 10'
theme.font_bold = 'SFNS Display Bold 10'

-- Colors Pallets

-- Accent
theme.accent = '#007af7'

-- Background
theme.background = '#000000' .. '66'

-- Transparent
theme.transparent = '#00000000'

local awesome_overrides = function(theme)
  --
end

return {
	theme = theme,
 	awesome_overrides = awesome_overrides
}
