local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi
local mat_list_item = require('widget.material.list-item')

local mat_slider = require('widget.material.slider')
local mat_icon_button = require('widget.material.icon-button')

local clickable_container = require('widget.material.clickable-container')
local icons = require('theme.icons')
local watch = require('awful.widget.watch')
local spawn = require('awful.spawn')


local osd_header = wibox.widget {
	text = 'Volume',
	font = 'SFNS Display Bold 12',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local osd_value = wibox.widget {
	text = '0%',
	font = 'SFNS Display Bold 12',
	align = 'center',
	valign = 'center',
	widget = wibox.widget.textbox
}

local slider_osd = wibox.widget {
	read_only = false,
	widget = mat_slider
}

-- Update volume level using slider value
slider_osd:connect_signal('property::value',
	function()
		spawn('amixer -D pulse sset Master ' .. slider_osd.value .. '%', false)

		-- Update textbox widget text
		osd_value.text = slider_osd.value .. '%'
	end
)

slider_osd:connect_signal(
	'button::press',
	function()
		slider_osd:connect_signal(
			'property::value',
			function()
				awesome.emit_signal('module::toggle_volume_osd', true)
			end
		)
	end
)

-- Update slider value
awesome.connect_signal('module::update_volume_osd', 
	function()
		awful.spawn.easy_async_with_shell("bash -c 'amixer -D pulse sget Master'", function(stdout)
			local mute = string.match(stdout, '%[(o%D%D?)%]')
			local volume = string.match(stdout, '(%d?%d?%d)%%')
			slider_osd:set_value(tonumber(volume))
		end, false)
	end
)

local icon =
	wibox.widget {
	image = icons.volume,
	widget = wibox.widget.imagebox
}

local button = mat_icon_button(icon)

local volume_slider_osd =
	wibox.widget {
	button,
	slider_osd,
	widget = mat_list_item
}


screen.connect_signal("request::desktop_decoration", function(s)

	-- Create the box
	local osd_height = dpi(96)
	local osd_width = dpi(300)

	local osd_offset = dpi(10)

	s.volume_osd_overlay = wibox
	{
		visible = nil,
		ontop = true,
		screen = s,
		type = "dock",
		height = osd_height,
		width = osd_width,
		bg = beautiful.transparent,
		x = (s.geometry.width - osd_width) - osd_offset,
		y = (s.geometry.height - osd_height) - osd_offset
	}

	-- Put its items in a shaped container
	s.volume_osd_overlay:setup {
		{
			{
				{
					layout = wibox.layout.align.horizontal,
					expand = 'none',
					osd_header,
					nil,
					osd_value
				},
				widget = mat_list_item
			},
			volume_slider_osd,
			layout = wibox.layout.fixed.vertical
		},
		bg = "#000000".. "66",
		-- The real, anti-aliased shape
		shape = gears.shape.rounded_rect,
		widget = wibox.container.background()
	}


	local hideOSD = gears.timer {
		timeout = 2,
		autostart = true,
		callback  = function()
			awful.screen.focused().volume_osd_overlay.visible = false
		end
	}

	-- Reset timer on mouse hover
	s.volume_osd_overlay:connect_signal('mouse::enter', function()
			hideOSD:again()
	 end
	)

	awesome.connect_signal('module::toggle_volume_osd', 
		function(bool)
			awful.screen.focused().volume_osd_overlay.visible = bool
				if bool then
					hideOSD:again()
					awesome.emit_signal('module::toggle_brightness_osd', false)
				else
					hideOSD:stop()
			end
		end
	)

end)