-- Libs
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local wibox = require("wibox")

local dpi = require('beautiful').xresources.apply_dpi

-- Dont't show tooltips
awful.titlebar.enable_tooltip = false

-- Titlebar size
local titlebar_size = beautiful.titlebar_size


-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)

	-- Buttons for moving/resizing functionality 
	local buttons = gears.table.join(
		awful.button(
			{}, 
			1, 
			function()
				c:emit_signal("request::activate", "titlebar", {raise = true})
				awful.mouse.client.move(c)
			end),
		awful.button(
			{}, 
			3, 
			function()
				c:emit_signal("request::activate", "titlebar", {raise = true})
				awful.mouse.client.resize(c)
			end)
	)


	-- Create and Decorate A Titlebar
	local decorate_titlebar = function(c, pos, bg, size)

		if pos == 'left' or pos == 'right' then

			-- Creates left or right titlebars

			awful.titlebar(c, {position = pos, bg = bg, size = size or titlebar_size}) : setup {
				{
					{
						awful.titlebar.widget.closebutton(c),
						awful.titlebar.widget.maximizedbutton(c),
						awful.titlebar.widget.minimizebutton (c),
						layout  = wibox.layout.fixed.vertical
					},
					{
						buttons = buttons,
						layout = wibox.layout.flex.vertical
					},
					{
						awful.titlebar.widget.floatingbutton (c),
						layout = wibox.layout.fixed.vertical
					},
					layout = wibox.layout.align.vertical
				},
				margins = dpi(3),
				widget = wibox.container.margin
			}

		elseif pos == 'top' or pos == 'bottom' then

			-- Creates top or bottom titlebars

			awful.titlebar(c, {position = pos, bg = bg, size = size or titlebar_size}) : setup {
				{
					{
						awful.titlebar.widget.closebutton(c),
						awful.titlebar.widget.maximizedbutton(c),
						awful.titlebar.widget.minimizebutton (c),
						layout  = wibox.layout.fixed.horizontal
					},
					{
						buttons = buttons,
						layout = wibox.layout.flex.horizontal
					},
					{
						awful.titlebar.widget.floatingbutton (c),
						layout = wibox.layout.fixed.horizontal
					},
					layout = wibox.layout.align.horizontal
				},
				margins = dpi(3),
				widget = wibox.container.margin
			}

		else

			-- Create left titlebar (default in this setup)

			awful.titlebar(c, {position = 'left', size = titlebar_size}) : setup {
				{
					{
						awful.titlebar.widget.closebutton(c),
						awful.titlebar.widget.maximizedbutton(c),
						awful.titlebar.widget.minimizebutton (c),
						layout  = wibox.layout.fixed.vertical
					},
					{
						buttons = buttons,
						layout = wibox.layout.flex.vertical
					},
					{
						awful.titlebar.widget.floatingbutton (c),
						layout = wibox.layout.fixed.vertical
					},
					layout = wibox.layout.align.vertical
				},
				margins = dpi(3),
				widget = wibox.container.margin
			}

		end

	end

	-- Generate a custom titlabar for each class and roles
	if c.class == "kitty" or c.class == "XTerm" then

		decorate_titlebar(c, 'left', '#000000AA', titlebar_size)

	elseif c.role == "GtkFileChooserDialog" or c.type == 'modal' or c.type == 'dialog' then

		-- Let's use the gtk themes bg_color as titlebar's bg
		-- isn't it neat? lol
		decorate_titlebar(c, 'left', beautiful.gtk.get_theme_variables().bg_color, titlebar_size)

	elseif c.class == "firefox" then

		decorate_titlebar(c, 'left', '#252525', titlebar_size)

	elseif c.class == "Gimp-2.10" then

		decorate_titlebar(c, 'left', beautiful.gtk.get_theme_variables().bg_color, titlebar_size)

	elseif c.class == "Subl3" then

		decorate_titlebar(c, 'left', '#252525', titlebar_size)

	else

		-- Default titlebar
		decorate_titlebar(c)

	end

end)

-- Client layout handler
require('module.titlebar-decorate-client')
