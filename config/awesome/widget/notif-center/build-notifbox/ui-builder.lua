-- Formats and builds UI widgets like font, imagebox and actions

--  #                                                     
--  #       # #####  #####    ##   #####  # ######  ####  
--  #       # #    # #    #  #  #  #    # # #      #      
--  #       # #####  #    # #    # #    # # #####   ####  
--  #       # #    # #####  ###### #####  # #           # 
--  #       # #    # #   #  #    # #   #  # #      #    # 
--  ####### # #####  #    # #    # #    # # ######  ####  

local wibox = require('wibox')
local beautiful = require('beautiful')
local naughty = require('naughty')
local gears = require('gears')


--  #     #                                           
--  #     # ###### #      #####  ###### #####   ####  
--  #     # #      #      #    # #      #    # #      
--  ####### #####  #      #    # #####  #    #  ####  
--  #     # #      #      #####  #      #####       # 
--  #     # #      #      #      #      #   #  #    # 
--  #     # ###### ###### #      ###### #    #  ####  

local dpi = require('beautiful').xresources.apply_dpi


local ui_noti_builder = {}

-- Notification icon container
notifbox_icon = function(ico_image)
	local noti_icon = wibox.widget {
		{
			id = 'icon',
			resize = true,
			forced_height = dpi(25),
			forced_width = dpi(25),
			widget = wibox.widget.imagebox
		},
		layout = wibox.layout.fixed.horizontal
	}
	noti_icon.icon:set_image(ico_image)
	return noti_icon
end

-- Notification title container
notifbox_title = function(title)
	return wibox.widget {
		text   = title,
		font   = 'SFNS Display Bold 12',
		align  = 'left',
		valign = 'center',
		widget = wibox.widget.textbox
	}
end

-- Notification message container
notifbox_message = function(msg)
	return wibox.widget {
		text   = msg,
		font   = 'SFNS Display Regular 11',
		align  = 'left',
		valign = 'center',
		widget = wibox.widget.textbox
	}
end

-- Notification app name container
notifbox_appname = function(app)
	return wibox.widget {
		text   = app,
		font   = 'SFNS Display Bold 12',
		align  = 'left',
		valign = 'center',
		widget = wibox.widget.textbox
	}
end

-- Notification actions container
notifbox_actions = function(n)
	actions_template = wibox.widget {
		notification = n,
		base_layout = wibox.widget {
			spacing        = dpi(5),
			layout         = wibox.layout.flex.vertical
		},
		widget_template = {
			{
				{
					{
						id     = 'text_role',
						font   = 'SFNS Display Regular 10',
						widget = wibox.widget.textbox
					},
						widget = wibox.container.place
				},
				bg                 = beautiful.groups_bg,
				shape              = gears.shape.rounded_rect,
				forced_height      = 30,
				widget             = wibox.container.background,
			},
			margins = 4,
			widget  = wibox.container.margin,
		},
		style = { underline_normal = false, underline_selected = true },
		widget = naughty.list.actions,
	}

	return actions_template
end


ui_noti_builder.notifbox_icon = notifbox_icon
ui_noti_builder.notifbox_title = notifbox_title
ui_noti_builder.notifbox_message = notifbox_message
ui_noti_builder.notifbox_appname = notifbox_appname
ui_noti_builder.notifbox_actions = notifbox_actions


return ui_noti_builder