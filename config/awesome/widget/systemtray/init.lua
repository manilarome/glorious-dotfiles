-------------------------------------------------
-- Toggle System tray
-------------------------------------------------

local awful = require('awful')
local wibox = require('wibox')
local clickable_container = require('widget.material.clickable-container')
local gears = require('gears')
local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/systemtray/icons/'

systemtray_button = true



local widget = wibox.widget {
	{
		id = 'icon',
		image = PATH_TO_ICONS .. 'right-arrow' .. '.svg',
		widget = wibox.widget.imagebox,
		resize = true
	},
	layout = wibox.layout.align.horizontal
}

local widget_button = clickable_container(wibox.container.margin(widget, dpi(8), dpi(8), dpi(8), dpi(8)))
widget_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				awesome.emit_signal("toggle_tray")
			end
			)
		)
	)


-- Listen to signal
awesome.connect_signal("toggle_tray", function()

	if screen.primary.systray then

		if screen.primary.systray.visible ~= true then

			widget.icon:set_image(gears.surface.load_uncached(PATH_TO_ICONS .. 'left-arrow' .. '.svg'))
		else

			widget.icon:set_image(gears.surface.load_uncached(PATH_TO_ICONS .. 'right-arrow' .. '.svg'))
		end

		screen.primary.systray.visible = not screen.primary.systray.visible
	end

end)

-- Update icon on start-up
if screen.primary.systray then
	
	if screen.primary.systray.visible then
		
		widget.icon:set_image(PATH_TO_ICONS .. 'right-arrow' .. '.svg')
		
	end
	
end


return widget_button

