local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local HOME = os.getenv('HOME')

local dpi = require('beautiful').xresources.apply_dpi

panel_visible = false

local right_panel = function(screen)

	-- Set right panel geometry
	local panel_width = dpi(350)
	local panel_x = screen.geometry.x + screen.geometry.width - panel_width

	local panel = wibox {
		ontop = true,
		screen = screen,
		type = 'dock',
		width = panel_width,
		height = screen.geometry.height,
		x = panel_x,
		y = screen.geometry.y,
		bg = beautiful.background,
		fg = beautiful.fg_normal
	}

	panel.opened = false

	screen.backdrop_rdb = wibox
	{
		ontop = true,
		screen = screen,
		bg = beautiful.transparent,
		type = 'utility',
		x = screen.geometry.x,
		y = screen.geometry.y,
		width = screen.geometry.width,
		height = screen.geometry.height
	}

	panel:struts
	{
		right = 0
	}
	
	openPanel = function()
		panel_visible = true
		awful.screen.focused().backdrop_rdb.visible = true
		awful.screen.focused().right_panel.visible = true
		panel:emit_signal('opened')
	end

	closePanel = function()
		panel_visible = false
		awful.screen.focused().right_panel.visible = false
		awful.screen.focused().backdrop_rdb.visible = false
		-- Change to notif mode on close
		_G.switch_mode('widgets_mode')
		panel:emit_signal('closed')
	end

	-- Hide this panel when app dashboard is called.
	function panel:HideDashboard()
		closePanel()
	end

	function panel:toggle()
		self.opened = not self.opened
		if self.opened then
			openPanel()
		else
			closePanel()
		end
	end


	function panel:switch_mode(mode)
		if mode == 'notif_mode' then
			-- Update Content
			panel:get_children_by_id('notif_id')[1].visible = true
			panel:get_children_by_id('widgets_id')[1].visible = false
		elseif mode == 'widgets_mode' then
			-- Update Content
			panel:get_children_by_id('notif_id')[1].visible = false
			panel:get_children_by_id('widgets_id')[1].visible = true
		end
	end

	screen.backdrop_rdb:buttons(
		awful.util.table.join(
			awful.button(
				{},
				1,
				function()
					panel:toggle()
				end
			)
		)
	)


	local separator = wibox.widget {
		orientation = 'horizontal',
		opacity = 0.0,
		forced_height = 15,
		widget = wibox.widget.separator,
	}

	local line_separator = wibox.widget {
		orientation = 'horizontal',
		forced_height = 15,
		span_ratio = 1.0,
		opacity = 0.90,
		color = beautiful.groups_bg,
		widget = wibox.widget.separator
	}

	panel : setup {
		expand = 'none',
		layout = wibox.layout.fixed.vertical,
		separator,
		{
			expand = 'none',
			layout = wibox.layout.align.horizontal,
			nil,
			require('widget.right-dashboard.subwidgets.panel-mode-switcher'),
			nil,
		},
		separator,
		line_separator,
		{
			layout = wibox.layout.stack,
			-- Widget Center
			{
				id = 'widgets_id',
				visible = true,
				layout = wibox.layout.fixed.vertical,
				separator,
				{
					{
						layout = wibox.layout.fixed.vertical,
						spacing = dpi(7),
						require('widget.user-profile'),
						require('widget.weather'),
						require('widget.email'),
						require('widget.social-media'),
						require('widget.calculator')
					},
					left = dpi(15),
					right = dpi(15),
					widget = wibox.container.margin
				},

			},

			-- Notification Center
			{
				id = 'notif_id',
				visible = false,
				separator,
				{
					{
						layout = wibox.layout.fixed.vertical,
						require('widget.notif-center'),
					},
					left = dpi(15),
					right = dpi(15),
					widget = wibox.container.margin
				},
				layout = wibox.layout.fixed.vertical,
			}

		}

	}


	return panel
end


return right_panel


