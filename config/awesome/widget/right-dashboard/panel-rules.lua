local awful = require('awful')
local wibox = require('wibox')

local right_panel = require('widget.right-dashboard.right-panel')

-- Create a wibox for each screen connected
screen.connect_signal("request::desktop_decoration", function(s)
	-- Create the right panel
	s.right_panel = right_panel(s)
end)


-- Save show_again value in focused screen
awful.screen.focused().show_again = false

-- Hide panel when clients go fullscreen
function update_rightbar_visibility()
	for s in screen do
		focused = awful.screen.focused()
		if focused.selected_tag then
			local fullscreen = focused.selected_tag.fullscreenMode
			if s.right_panel then
				if fullscreen and focused.right_panel.visible then
					focused.right_panel:toggle()
					focused.show_again = true
				elseif not fullscreen and not focused.right_panel.visible and focused.show_again then
					focused.right_panel:toggle()
					focused.show_again = false
				end
			end
		end
	end
end


tag.connect_signal(
	'property::selected',
	function(t)
		update_rightbar_visibility()
	end
)

client.connect_signal(
	'property::fullscreen',
	function(c)
		if c.first_tag then
			c.first_tag.fullscreenMode = c.fullscreen
		end
		update_rightbar_visibility()
	end
)

client.connect_signal(
	'unmanage',
	function(c)
		if c.fullscreen then
			c.screen.selected_tag.fullscreenMode = false
			update_rightbar_visibility()
		end
	end
)
