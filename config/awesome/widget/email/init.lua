local awful = require('awful')
local beautiful = require('beautiful')
local gears = require('gears')
local wibox = require('wibox')

local dpi = require('beautiful').xresources.apply_dpi
local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/email/icons/'

local naughty = require('naughty')

local secrets = require('secrets')

local mail_counter = 0

local email_state = nil


-- Credentials
local email_account   = secrets.email.address
local app_password    = secrets.email.app_password
local imap_server     = secrets.email.imap_server
local port            = secrets.email.port


local email_icon_widget = wibox.widget {
	{
		id = 'icon',
		image = PATH_TO_ICONS .. 'email' .. '.svg',
		resize = true,
		forced_height = dpi(45),
		forced_width = dpi(45),
		widget = wibox.widget.imagebox,
	},
	layout = wibox.layout.fixed.horizontal
}

-- local email_header = wibox.widget {
--   text   = "Email",
--   font   = 'SFNS Display Regular 14',
--   align  = 'center',
--   valign = 'center',
--   widget = wibox.widget.textbox
-- }

local email_unread_text = wibox.widget {
	text = 'Unread emails:',
	font = 'SFNS Display Bold 10',
	align = 'center',
	valign = 'center',
	widget = wibox.widget.textbox
}

local email_count = wibox.widget {
	text = '0',
	font = 'SFNS Display Regular 10',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local email_provider = wibox.widget {
	text = 'Email Service Provider:',
	font = 'SFNS Display Bold 10',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}


local provider = wibox.widget {
	text = '127.0.0.1',
	font = 'SFNS Display Regular 10',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local email_recent_from = wibox.widget {
	-- text = 'From:',
	font = 'SFNS Display Regular 10',
	markup = '<span font="SFNS Display Bold 10">From: </span>loading@stdout.sh',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local email_recent_subject = wibox.widget {
	markup = '<span font="SFNS Display Bold 10">Subject: </span>Loading data',
	-- font = 'SFNS Display Regular 10',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

-- Show email count
local check_count = [[
# A simple python script to get email count wrapped inside bash wrapped inside lua lol
# Make sure to encrypt this
python3 - <<END
import imaplib
import re

M=imaplib.IMAP4_SSL("]] .. imap_server .. [[", ]] .. port .. [[)
M.login("]] .. email_account .. [[","]] .. app_password .. [[")

status, counts = M.status("INBOX","(MESSAGES UNSEEN)")

if status == "OK":
	unread = re.search(r'UNSEEN\s(\d+)', counts[0].decode('utf-8')).group(1)
else:
	unread = "N/A"

print(unread)
END
]]


-- Show unread emails
local read_unread = [[
# A simple python script to get unread emails wrapped inside bash wrapped inside lua lol
# Make sure to encrypt this
python3 - <<END
import imaplib
import email
import datetime

def process_mailbox(M):
	rv, data = M.search(None, "(UNSEEN)")
	if rv != 'OK':
		print ("No messages found!")
		return

	for num in data[0].split():
		rv, data = M.fetch(num, '(BODY.PEEK[])')
		if rv != 'OK':
			print ("ERROR getting message", num)
			return

		msg = email.message_from_bytes(data[0][1])
		print ('From:', msg['From'])
		print ('Subject: %s' % (msg['Subject']))
		date_tuple = email.utils.parsedate_tz(msg['Date'])
		if date_tuple:
			local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
			print ("Local Date:", local_date.strftime("%a, %d %b %Y %H:%M:%S"))
			# with code below you can process text of email
			# if msg.is_multipart():
			#     for payload in msg.get_payload():
			#         if payload.get_content_maintype() == 'text':
			#             print  payload.get_payload()
			#         else:
			#             print msg.get_payload()


M=imaplib.IMAP4_SSL("]] .. imap_server .. [[", ]] .. port .. [[)
M.login("]] .. email_account .. [[","]] .. app_password .. [[")

rv, data = M.select("INBOX")
if rv == 'OK':
		process_mailbox(M)
M.close()
M.logout()

END
]]


-- Show recent received email
local read_recent_datails = [[
# A simple python script to get unread emails wrapped inside bash wrapped inside lua lol
# Make sure to encrypt this
python3 - <<END
import imaplib
import email
import datetime

def process_mailbox(M):
	rv, data = M.search(None, "(UNSEEN)")
	if rv != 'OK':
		print ("No messages found!")
		return

	for num in reversed(data[0].split()):
		rv, data = M.fetch(num, '(BODY.PEEK[])')
		if rv != 'OK':
			print ("ERROR getting message", num)
			return

		msg = email.message_from_bytes(data[0][1])
		print ('From:', msg['From'])
		print ('Subject: %s' % (msg['Subject']))
		return


M=imaplib.IMAP4_SSL("]] .. imap_server .. [[", ]] .. port .. [[)
M.login("]] .. email_account .. [[","]] .. app_password .. [[")

rv, data = M.select("INBOX")
if rv == 'OK':
		process_mailbox(M)
M.close()
M.logout()

END
]]




-- Widget layout
local email_report = wibox.widget{
	expand = 'none',
	layout = wibox.layout.fixed.vertical,
	-- {
	--   {
	--     email_header,
	--     margins = dpi(10),
	--     widget = wibox.container.margin
	--   },
	--   bg = beautiful.groups_title_bg,
	--   shape = function(cr, width, height)
	--     gears.shape.partially_rounded_rect(cr, width, height, true, true, false, false, beautiful.groups_radius) end,
	--   widget = wibox.container.background
	-- },
	{
		{
			{
				layout = wibox.layout.fixed.horizontal,
				spacing = dpi(5),
				{
					{
						layout = wibox.layout.align.vertical,
						expand = "none",
						nil,
						email_icon_widget,
						nil,
					},
					margins = dpi(5),
					widget = wibox.container.margin
				},
				{
					{
						layout = wibox.layout.fixed.vertical,
						{
							layout = wibox.layout.fixed.horizontal,
							spacing = dpi(5),
							email_provider,
							provider
						},
						{
							layout = wibox.layout.fixed.horizontal,
							spacing = dpi(5),
							email_unread_text,
							email_count
						},
						email_recent_from,
						email_recent_subject

					},
					margins = dpi(4),
					widget = wibox.container.margin
				},
			},
			margins = dpi(5),
			widget = wibox.container.margin
		},
		bg = beautiful.groups_bg,
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, beautiful.groups_radius) end,
		widget = wibox.container.background
	},
}



-- Create a notification
local notify_new_email = function(count)
	if tonumber(count) > tonumber(mail_counter) then
		mail_counter = count
		naughty.notification({ 
			title = "Message received!",
			message = "You have an unread email!",
			app_name = 'Email',
			icon = PATH_TO_ICONS .. 'email' .. '.svg'
		})
	else
		mail_counter = count
	end
end

-- Set text
local set_error_msg = function(status)

	if status == 'no-credentials' then
		email_recent_from.markup = '<span font="SFNS Display Bold 10">From: </span>' .. 'message@stderr.sh'
		email_recent_subject.markup = '<span font="SFNS Display Bold 10">Subject: </span>' .. 'Credentials are missing!'
		return
	end

	if status == 'no-network' then
		email_recent_from.markup = '<span font="SFNS Display Bold 10">From: </span>' .. 'message@stderr.sh'
		email_recent_subject.markup = '<span font="SFNS Display Bold 10">Subject: </span>' .. 'Check network connection!'
		return
	end

end


local set_email_details = function()
	awful.spawn.easy_async_with_shell(read_recent_datails, function(stdout)
		if stdout:match('%W') then
			local text_from = stdout:match('From: (.*)Subject:'):gsub('%\n','')
			local text_subject = stdout:match('Subject: (.*)'):gsub('%\n','')
			
			if(text_subject:len() > 25) then
				-- Trim file to 26 characters
				text_subject = text_subject:sub(1,25) .. '...'
			end

			-- Only get the email address
			text_from = text_from:match('<(.*)>')

			email_recent_from.markup = '<span font="SFNS Display Bold 10">From: </span>' .. text_from
			email_recent_subject.markup = '<span font="SFNS Display Bold 10">Subject: </span>' .. text_subject
		end
	end)
end


local set_no_email_details = function()
	awful.spawn.easy_async_with_shell(read_recent_datails, function(stdout)

		email_recent_from.markup = '<span font="SFNS Display Bold 10">From: </span>' .. 'empty@stdout.sh'
		email_recent_subject.markup = '<span font="SFNS Display Bold 10">Subject: </span>' .. 'Empty inbox'
	end)
end


local update_widget = function()
	awful.spawn.easy_async_with_shell(check_count, function(stdout)
		if tonumber(stdout) ~= nil then
			unread_count = stdout:gsub('%\n','')

			if tonumber(unread_count) > 0 then
				-- Get from and Subject
				set_email_details()
			else
				-- Set empty messages
				set_no_email_details()
			end

			-- Update unread count
			email_count.text = tonumber(unread_count)
	
			-- Notify
			notify_new_email(unread_count)
		else
			-- Send no network err msg
			set_error_msg('no-network')
		end
	end)
end


local check_credentials = function()
	if email_account == '' or app_password == '' or imap_server == '' or port == '' then
		-- Send no credential/s err msg
		set_error_msg('no-credentials')
	else
		-- If there's credentials, update widget
		update_widget()
	end
end


-- Updater
local update_widget_timer = gears.timer {
	timeout = 60,
	autostart = true,
	call_now = true,
	callback  = function()
		-- Check if there's a credentials
		check_credentials() 
	end
}

-- Set provider based on imapserver
if secrets.email.imap_server:match('gmail') then

	provider.markup = 'Gmail'

elseif secrets.email.imap_server:match('yahoo') then

	provider.markup = 'Yahoo'

elseif secrets.email.imap_server:match('outlook') then

	provider.markup = 'Outlook'
	
end

-- A tooltip
local read_emails = awful.tooltip
{
	text = 'Loading...',
	objects = {email_icon_widget},
	mode = 'outside',
	align = 'right',
	preferred_positions = {'right', 'left', 'top', 'bottom'},
	margin_leftright = dpi(8),
	margin_topbottom = dpi(8)
}


-- Update tooltip
local show_emails = function()
	awful.spawn.easy_async_with_shell(read_unread, function(stdout, stderr, reason, exit_code)
		if (stdout:match("%W")) then
			if stdout ~= nil then
				read_emails.text = stdout:gsub('\n$', '')
			else
				read_emails.text = 'Loading...'
			end
		else
			read_emails.text = 'No unread emails...'
		end
	end
	)
end

-- Show unread emails on hover
email_icon_widget:connect_signal("mouse::enter", function() show_emails() end)


return email_report
