--  #     #                        
--  ##   ## #    #  ####  #  ####  
--  # # # # #    # #      # #    # 
--  #  #  # #    #  ####  # #      
--  #     # #    #      # # #      
--  #     # #    # #    # # #    # 
--  #     #  ####   ####  #  ####  

local gears = require('gears')
local awful = require('awful')
local wibox = require('wibox')

local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/music/icons/'
local clickable_container = require('widget.material.clickable-container')

-- Instantiate music box pop-up
local music_box = require('widget.music.music-box')

-- Instantiate toggle_music_box function
local toggle_music_box = music_box.toggle_music_box


local return_button = function()

	local widget = wibox.widget {
		{
			id = 'icon',
			image = PATH_TO_ICONS .. 'music' .. '.svg',
			widget = wibox.widget.imagebox,
			resize = true
		},
		layout = wibox.layout.align.horizontal
	}

	local widget_button = clickable_container(
		wibox.container.margin(
			widget, dpi(8), dpi(8), dpi(8), dpi(8)
		)
	)

	-- The tooltip
	local music_tooltip =  awful.tooltip
	{
		objects = {widget_button},
		text = 'None',
		mode = 'outside',
		margin_leftright = dpi(8),
		margin_topbottom = dpi(8),
		align = 'right',
		preferred_positions = {'right', 'left', 'top', 'bottom'}
	}

	-- Keypress event
	widget_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					music_tooltip.visible = false
					toggle_music_box()
				end
				)
			)
		)


	-- Update tooltip text on hover
	widget_button:connect_signal("mouse::enter", function() 
		awful.spawn.easy_async_with_shell('mpc status', function(stdout) 
			music_tooltip.text = string.gsub(stdout, '\n$', '')
		end, false)
	end)


	return widget_button

end


return return_button