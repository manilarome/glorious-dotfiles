--  #     #                           ######                
--  ##   ## #    #  ####  #  ####     #     #  ####  #    # 
--  # # # # #    # #      # #    #    #     # #    #  #  #  
--  #  #  # #    #  ####  # #         ######  #    #   ##   
--  #     # #    #      # # #         #     # #    #   ##   
--  #     # #    # #    # # #    #    #     # #    #  #  #  
--  #     #  ####   ####  #  ####     ######   ####  #    # 

-- Creates the music box widget here

local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi

local clickable_container = require('widget.material.clickable-container')
local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/music/icons/'

local mat_list_item = require('widget.material.list-item')

local apps = require('configuration.apps')

-- A table that will contain some functions
local music_func = {}

screen.connect_signal("request::desktop_decoration", function(s)

	-- Set music box geometry
	local music_box_margin = dpi(5)
	local music_box_height = dpi(375)
	local music_box_width = dpi(260)


	s.musicpop = awful.popup {
		widget = {
		  -- Removing this block will cause an error...
		},
		ontop = true,
		visible = false,
		type = 'dock',
		screen = s,
		width = music_box_width,
		height = music_box_height,
		maximum_width = music_box_width,
		maximum_height = music_box_height,
		offset = dpi(5),
		shape = gears.shape.rectangle,
		bg = beautiful.transparent,
		preferred_anchors = 'middle',
		preferred_positions = {'left', 'right', 'top', 'bottom'},

	}

	s.musicpop : setup {
		{
			layout = wibox.layout.fixed.vertical,
			spacing = dpi(8),
			{
				require('widget.music.content.album-cover'),
				-- Yes I know how to do this in a one liner, I'm just changing my *style* yow
				top = dpi(15),
				left = dpi(15),
				right = dpi(15),
				bottom = dpi(5),
				widget = wibox.container.margin,
			},
			{
				spacing = dpi(0),
				layout = wibox.layout.fixed.vertical,
				{
					spacing = dpi(4),
					layout = wibox.layout.fixed.vertical,
					{
						require('widget.music.content.progress-bar'),
					  	left = dpi(15),
					  	right = dpi(15),
					  	widget = wibox.container.margin,
					},
					{
						require('widget.music.content.track-time').time_track,
				  		left = dpi(15),
				  		right = dpi(15),
					  	widget = wibox.container.margin,
					},
				},
				{
					require('widget.music.content.song-info').music_info,
			  		left = dpi(15),
			  		right = dpi(15),
					widget = wibox.container.margin,
				},
				{
					require('widget.music.content.media-buttons').navigate_buttons,
			  		left = dpi(15),
					right = dpi(15),
			  		widget = wibox.container.margin,
				},
				{
					require('widget.music.content.volume-slider').slider_volume,
				  	left = dpi(15),
				  	right = dpi(15),
				  	widget = wibox.container.margin,
				},
		  	},

		},
		-- The real background color
		bg = beautiful.background,
		-- The real, anti-aliased shape
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect( cr, width, height, true, true, true, true, beautiful.groups_radius) 
		end,
		widget = wibox.container.background()
	}

	s.backdrop_music = wibox {
		ontop = true,
		visible = false,
		screen = s,
		bg = beautiful.transparent,
		type = 'utility',
		x = s.geometry.x,
		y = s.geometry.y + dpi(30),
		width = s.geometry.width,
		height = s.geometry.height - dpi(30)
	}

	require('widget.music.get-margin')(s, music_box_width, 15)

	function toggle_music_box()

		local focused = awful.screen.focused()
		local music_box = focused.musicpop
		local music_backdrop = focused.backdrop_music

		if music_box.visible then
		  	music_backdrop.visible = not music_backdrop.visible
			music_box.visible = not music_box.visible

		else
			music_backdrop.visible = true
			music_box.visible = true

			awful.placement.top_right(music_box, { margins = { 
				top = focused.margin_y or dpi(35), 
				right = focused.margin_x or dpi(95)
			}, 
			parent = focused 
			})

		end

	end


	s.backdrop_music:buttons(
		awful.util.table.join(
		  awful.button(
			{},
			1,
			function()
			  toggle_music_box()
			end
			)
		  )
		)

	-- Intantiate updater
	require('widget.music.mpd-music-updater')

end)

music_func.toggle_music_box = toggle_music_box

return music_func 