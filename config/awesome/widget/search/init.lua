-------------------------------------------------
-- Rofi toggler widget for Awesome Window Manager
-- Shows the application list
-- Use rofi-git master branch
-------------------------------------------------

local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')

local dpi = require('beautiful').xresources.apply_dpi
local clickable_container = require('widget.material.clickable-container')

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/search/icons/'

local apps = require('configuration.apps')

local return_button = function()

	local widget = wibox.widget {
		{
			id = 'icon',
			image = PATH_TO_ICONS .. 'search' .. '.svg',
			widget = wibox.widget.imagebox,
			resize = true
		},
		layout = wibox.layout.align.horizontal
	}

	local widget_button = clickable_container(wibox.container.margin(widget, dpi(9), dpi(9), dpi(9), dpi(9)))
	widget_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					awful.spawn(apps.default.rofiappmenu)
				end
			)
		)
	)


	return widget_button
end

return return_button