-------------------------------------------------
-- Package Updater Widget for Awesome Window Manager
-- Shows the package updates information in Arch Linux
-------------------------------------------------

local awful = require('awful')
local naughty = require('naughty')
local wibox = require('wibox')
local gears = require('gears')

local watch = require('awful.widget.watch')
local clickable_container = require('widget.material.clickable-container')
local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/package-updater/icons/'

local update_available = false
local number_of_updates_available = nil
local update_package = nil

local return_button = function()

	local widget = wibox.widget {
		{
			id = 'icon',
			widget = wibox.widget.imagebox,
			image = PATH_TO_ICONS .. 'package' .. '.svg',
			resize = true
		},
		layout = wibox.layout.align.horizontal
	}

	local widget_button = clickable_container(wibox.container.margin(widget, dpi(9), dpi(9), dpi(9), dpi(9)))
	widget_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					
					if update_available then
						awful.spawn('pamac-manager --updates', false)
					
					else
						awful.spawn('pamac-manager', false)
					
					end
				end
			)
		)
	)


	-- Tooltip
	awful.tooltip(
		{
			objects = {widget_button},
			mode = 'outside',
			align = 'right',
			margin_leftright = dpi(8),
			margin_topbottom = dpi(8),
			timer_function = function()

				if update_available then
					return update_package:gsub('\n$', '')
				else
					return 'We are up-to-date!'
				end
			
			end,
			preferred_positions = {'right', 'left', 'top', 'bottom'}
		}
	)


	watch('pamac checkupdates', 60, function(_, stdout)

		number_of_updates_available = tonumber(stdout:match('.-\n'):match('%d*'))
		update_package = stdout
		
		local icon_name
			
		if number_of_updates_available ~= nil then
			
			update_available = true
			icon_name = 'package-up'
			
		else

			update_available = false
			icon_name = 'package'
			
		end

		widget.icon:set_image(PATH_TO_ICONS .. icon_name .. '.svg')

		collectgarbage('collect')


	end)

	return widget_button

end

return return_button