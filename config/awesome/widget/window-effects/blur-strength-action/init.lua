local wibox = require('wibox')
local awful = require('awful')

local spawn = require('awful.spawn')

local icons = require('theme.icons')
local watch = require('awful.widget.watch')

local mat_list_item = require('widget.material.list-item')
local mat_slider = require('widget.material.slider')
local mat_icon_button = require('widget.material.icon-button')

local slider = wibox.widget {
	read_only = false,
	widget = mat_slider
}

local get_blur_strength = [[
grep -F 'strength =' .config/awesome/configuration/picom.conf | awk 'NR==1 {printf $3}' | tr -d ';'
]]

local update_slider_value = function()
	awful.spawn.easy_async_with_shell(get_blur_strength, function(stdout)
		blur_strength = tonumber(stdout) / 20 * 100
		slider:set_value(tonumber(blur_strength))
	end, false)
end

-- Update slider value
update_slider_value()

local adjust_blur = function(power)
	local adjust_blur_cmd = [[
		picom_dir=/home/gerome/.config/awesome/configuration/picom.conf 
		sed -i 's/.*strength = .*/  strength = ]] .. power .. [[;/g' "${picom_dir}"
	]]

		awful.spawn.easy_async_with_shell(adjust_blur_cmd, function(stdout, stderr)
		end, false)
end


slider:connect_signal(
	'property::value',
	function()
		strength = slider.value / 50 * 10
		adjust_blur(strength)
	end
)

-- Adjust slider value to change blur strength
awesome.connect_signal('widget::increase_blur', function() 

	-- On startup, the slider.value returns nil so...
	if slider:get_value() == nil then
		return
	end
 
	local blur_value = slider:get_value() + 5

	-- No more than 100!
	if blur_value > 100 then
		slider:set_value(100)
		return
	end

	slider:set_value(blur_value)
end)

-- Decrease blur
awesome.connect_signal('widget::decrease_blur', function() 
	
	-- On startup, the slider.value returns nil so...
	if slider:get_value() == nil then
		return
	end

	local blur_value = slider:get_value() - 5

	-- No negatives!
	if blur_value < 0 then
		slider:set_value(0)
		return
	end

	slider:set_value(blur_value)
end)


local icon =
	wibox.widget {
	image = icons.effects,
	widget = wibox.widget.imagebox
}

local button = mat_icon_button(icon)

local volume_setting = wibox.widget {
	button,
	slider,
	widget = mat_list_item
}

return volume_setting
