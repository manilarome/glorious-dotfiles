-------------------------------------------------
-- Battery Widget for Awesome Window Manager
-- Shows the battery status using the ACPI tool
-- More details could be found here:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/battery-widget

-- @author Pavel Makhov
-- @copyright 2017 Pavel Makhov
-------------------------------------------------

local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local naughty = require('naughty')
local watch = require('awful.widget.watch')

local clickable_container = require('widget.material.clickable-container')
local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/battery/icons/'


local return_button = function()

    local battery_imagebox = wibox.widget {
        {
            id = 'icon',
            image = PATH_TO_ICONS .. 'battery' .. '.svg',
            widget = wibox.widget.imagebox,
            resize = true
        },
        layout = wibox.layout.fixed.horizontal
    }

    local battery_percentage = wibox.widget {
        id = 'percent_text',
        text = '100%',
        font = 'SFNS Display Bold 10',
        visible = true,
        widget = wibox.widget.textbox
    }


    local battery_widget = wibox.widget {
        layout = wibox.layout.fixed.horizontal,
        spacing = dpi(0),
        battery_imagebox,
        battery_percentage
    }



    local battery_button = clickable_container(wibox.container.margin(battery_widget, dpi(9), dpi(9), dpi(9), dpi(9)))
    battery_button:buttons(
        gears.table.join(
            awful.button(
                {},
                1,
                nil,
                function()
                    awful.spawn('xfce4-power-manager-settings')
                end
            )
        )
    )

    local battery_tooltip =  awful.tooltip
    {
        objects = {battery_button},
        text = 'None',
        mode = 'outside',
        align = 'right',
        argin_leftright = dpi(8),
        margin_topbottom = dpi(8),
        preferred_positions = {'right', 'left', 'top', 'bottom'}
    }


    local function show_battery_warning()
        naughty.notification ({
            icon = PATH_TO_ICONS .. 'battery-alert.svg',
            message = 'Hey, we have a problem.',
            title = 'Battery is dying!',
            app_name = 'System',
            preset = naughty.config.presets.critical
        })
    end

    local last_battery_check = os.time()

    watch(
        'acpi -i',
        10,
        function(_, stdout)
            local batt_icon_name = 'battery'
            local battery_info = {}
            local capacities = {}

            -- Update tooltip text
            battery_tooltip.text = string.gsub(stdout, '\n$', '')

            -- Update percentage textbox
            awful.spawn.easy_async_with_shell("acpi -i | awk \'NR==1 {print $4}\' | tr -d '.,'", function (stdout)

                if stdout:match('%W') then
                    battery_percentage.visible = true
                    battery_widget.spacing = dpi(5)
                    battery_percentage:set_text(tostring(stdout))
                else
                    battery_tooltip:set_text('Battery is not detected!')
                end

            end, false)


            for s in stdout:gmatch('[^\r\n]+') do
                local status, charge_str, time = string.match(s, '.+: (%a+), (%d?%d?%d)%%,?.*')
                if status ~= nil then
                    table.insert(battery_info, {status = status, charge = tonumber(charge_str)})
                else
                    local cap_str = string.match(s, '.+:.+last full capacity (%d+)')
                    table.insert(capacities, tonumber(cap_str))
                end
            end

            local capacity = 0
            for _, cap in ipairs(capacities) do
                capacity = capacity + cap
            end

        local charge = 0
        local status


        for i, batt in ipairs(battery_info) do
            if batt.charge >= charge then
                status = batt.status -- use most charged battery status
                -- this is arbitrary, and maybe another metric should be used
            end
            charge = charge + batt.charge * capacities[i]
        end

        charge = charge / capacity

        if (charge >= 0 and charge < 15) then
            if status ~= 'Charging' and os.difftime(os.time(), last_battery_check) > 300 then
                -- if 5 minutes have elapsed since the last warning
                last_battery_check = _G.time()

                show_battery_warning()
            end
        end


        -- Set battery icon
        if status == nil then
            batt_icon_name = batt_icon_name .. '-unknown'
        elseif status == 'Charging' or status == 'Full' then
            batt_icon_name = batt_icon_name .. '-charging'

        else
            local rounded_charge = math.floor(charge / 10) * 10
            if (rounded_charge == 0) then
                batt_icon_name = batt_icon_name .. '-outline'
            elseif (rounded_charge ~= 100) then
                batt_icon_name = batt_icon_name .. '-' .. rounded_charge
            end

        end

        battery_imagebox.icon:set_image(PATH_TO_ICONS .. batt_icon_name .. '.svg')

        collectgarbage('collect')
        
        end
    )

    return battery_button


end


return return_button