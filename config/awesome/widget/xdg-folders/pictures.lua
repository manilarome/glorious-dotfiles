local wibox = require('wibox')
local awful = require('awful')
local naughty = require('naughty')

local clickable_container = require('widget.material.clickable-container')
local gears = require('gears')
local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/xdg-folders/icons/'

local pic_widget =	wibox.widget {
	{
		image = PATH_TO_ICONS .. 'folder-pictures' .. '.svg',
		resize = true,
		widget = wibox.widget.imagebox
	},
	layout = wibox.layout.align.horizontal
}

local pic_button = clickable_container(wibox.container.margin(pic_widget, dpi(8), dpi(8), dpi(8), dpi(8)))
pic_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				awful.spawn({'xdg-open', HOME .. '/Pictures'}, false)
			end
		)
	)
)

awful.tooltip(
	{
		objects = {pic_button},
		mode = 'outside',
		align = 'right',
		text = 'Pictures',
		margin_leftright = dpi(8),
		margin_topbottom = dpi(8),    
		preferred_positions = {'right', 'left', 'top', 'bottom'}
	}
)


return pic_button
