-- User profile widget
-- Optional dependency:
--    mugshot (use to update profile picture and information)


local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi

local clickable_container = require('widget.material.clickable-container')

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/user-profile/icons/'

local PATH_TO_USERICON = '/var/lib/AccountsService/icons/'


local profile_imagebox = wibox.widget {
	{
		id = 'icon',
		forced_height = dpi(70),
		image = PATH_TO_ICONS .. 'user' .. '.svg',
		clip_shape = gears.shape.circle,
		widget = wibox.widget.imagebox,
		resize = true
	},
	layout = wibox.layout.align.horizontal
}

local profile_name = wibox.widget {
	font = "SFNS Display Bold 16",
	markup = 'User',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local distro_name = wibox.widget {
	font = "SFNS Display Regular 12",
	markup = 'GNU/Linux',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local kernel_version = wibox.widget {
	font = "SFNS Display Regular 10",
	markup = 'Linux',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

local uptime_time = wibox.widget {
	font = "SFNS Display Regular 10",
	markup = 'up 1 minute',
	align = 'left',
	valign = 'center',
	widget = wibox.widget.textbox
}

-- Get profile picture

local user_jpg_checker = [[
if test -f ]] .. PATH_TO_ICONS .. 'user.jpg' .. [[; then print 'yes'; fi
]]

awful.spawn.easy_async_with_shell(user_jpg_checker, function(already)

	if already:match('yes') then
		
		-- Update imagebox
		profile_imagebox.icon:set_image(PATH_TO_ICONS .. 'user.jpg')
	
	else

		-- Get username first
		awful.spawn.easy_async_with_shell('whoami', function(stdout)

			local username = stdout:gsub('%W', '')

			local check_profile_pic_cmd = [[
			if test -f ]] .. PATH_TO_USERICON .. username .. [[; then print 'detected'; fi
			]]


			-- Check AccountsService if user profile image is available
			awful.spawn.easy_async_with_shell(check_profile_pic_cmd, function(status) 


			-- If image exist
			if status:match('detected') then

				-- Copy it to widget icon folder as `user.jpg`
				copy_profile_image_cmd = [[
				cp ]] .. PATH_TO_USERICON .. username .. [[ ]] .. PATH_TO_ICONS .. [[user.jpg
				]]

				-- Copy
				awful.spawn(copy_profile_image_cmd)


				-- Update imagebox with a delay
				gears.timer {
					timeout = 2,
					autostart = true,
					call_now = false,
					single_shot = true,
					callback = function()
						profile_imagebox.icon:set_image(PATH_TO_ICONS .. 'user.jpg')
					end
				}

			else

				-- No image in AccountsService, use the default picture
				profile_imagebox.icon:set_image(PATH_TO_ICONS .. 'user' .. '.svg')

			end

			end, false)
		end, false)
	end
end, false)


-- Get username

awful.spawn.easy_async_with_shell('whoami', function(stdout) 

	i_am_who = stdout:gsub('%W', '')

	-- Capitalize first letter
	-- i_am_who = i_am_who:sub(1,1):upper() .. i_am_who:sub(2)

	awful.spawn.easy_async_with_shell('hostname', function(host)

		host_who = host:gsub('%W', '')
		
		profile_name.markup = i_am_who .. '@' .. host_who


	end, false)


end, false)

-- Get distro name

local get_distro_name_cmd = [[
cat /etc/os-release | awk 'NR==1'| awk -F '"' '{print $2}'
]]

awful.spawn.easy_async_with_shell(get_distro_name_cmd, function(out)

	distroname = out:gsub('%\n', '')
	distro_name.markup = distroname

end)

-- Get kernel version
local get_kernel_cmd = [[
cat /proc/version | cut -d " " -f '3'
]]

awful.spawn.easy_async_with_shell(get_kernel_cmd, function(out)

	kname = out:gsub('%\n', '')
	kernel_version.markup = kname

end)


-- Get uptime

gears.timer{
	timeout = 600,
	autostart = true,
	call_now = true,
	callback = function()
		awful.spawn.easy_async_with_shell("uptime -p", function(out)

			uptime = out:gsub('%\n','')
			uptime_time.markup = uptime
			
		end)
	end
}


local user_profile = wibox.widget {
	expand = 'none',
	{
		{
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(10),
			{
				profile_imagebox,
				margins = dpi(3),
				widget = wibox.container.margin,
			},
			{
				-- expand = 'none',
				layout = wibox.layout.align.vertical,
				expand = 'none',
				nil,
				{
					profile_name,
					distro_name,
					kernel_version,
					uptime_time,
					layout = wibox.layout.fixed.vertical
				},
				nil
				
			},
		},
		margins = dpi(10),
		widget = wibox.container.margin,
	},
	bg = beautiful.groups_bg,
	shape = function(cr, width, height) gears.shape.rounded_rect(cr, width, height, beautiful.groups_radius) end,
	widget = wibox.container.background
}



return user_profile
