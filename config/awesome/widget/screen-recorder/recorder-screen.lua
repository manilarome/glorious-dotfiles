local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi

local clickable_container = require('widget.material.clickable-container')

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/screen-recorder/icons/'

local record_tbl = {}

local status_recording = false

local status_countdown = false

local status_audio = false

local ffmpeg_pid = 0


local record_resolution = nil
local record_fps = nil
local record_directory = nil
local record_input_device = nil
local record_mic_power = nil

config_getter = function(res, fps, dir, dev, mic_power)
	record_resolution = res
	record_fps = fps
	record_directory = dir
	record_input_device = dev
	record_mic_power = mic_power
end


local recorder_button_imagebox = wibox.widget {
	{
		id = 'button_icon',
		image = PATH_TO_ICONS .. 'recorder-off' .. '.svg',
		widget = wibox.widget.imagebox,
		resize = true
	},
	layout = wibox.layout.fixed.horizontal
}

local separator =  wibox.widget {
	orientation = 'horizontal',
	forced_height = dpi(1),
	forced_width = dpi(1),
	span_ratio = 1.0,
	opacity = 0.05,
	widget = wibox.widget.separator
}


local recording_status_imagebox = wibox.widget {
	nil,
	{
		id = 'icon',
		forced_height = dpi(100),
		image = PATH_TO_ICONS .. 'start-recording' .. '.svg',
		resize = true,
		widget = wibox.widget.imagebox
	},
	nil,
	expand = 'none',
	layout = wibox.layout.align.horizontal
}

local recording_header = wibox.widget {
	id = 'header_text',
	font = 'SFNS Display Bold 19',
	text = 'Screen Recorder',
	align = 'center',
	valign = 'center',
	widget = wibox.widget.textbox
}


local recording_toggle = wibox.widget {
	{
		id = 'toggle_text',
		font = 'SFNS Display Regular 14',
		text = 'Start Recording',
		align = 'center',
		valign = 'center',
		widget = wibox.widget.textbox
	},
	margins = dpi(5),
	widget = wibox.container.margin
}

local counter_text = wibox.widget {
	id = 'counter',
	font = 'SFNS Display Bold 64',
	text = '4',
	align = 'center',
	valign = 'bottom',
	opacity = 0.0, 
	widget = wibox.widget.textbox
}

-- Countdown timer
local counter_timer = function()

	status_countdown = true

	local seconds = 4
	
	countdown_timer = gears.timer.start_new(1, function()
		if seconds == 0 then
			awesome.emit_signal('module::screen_recorder', 'start_recording')
			counter_text:set_text(tostring(seconds))
			return false 
		else
			counter_text.opacity = 1.0
			counter_text:set_text(tostring(seconds))
		end
	
		seconds = seconds - 1
	
		return true
	end)

end


-- Killall ffmpeg command
local killall_ffmpeg = [[
ps x | grep "ffmpeg -video_size" | grep -v grep | awk '{print $1}' | xargs kill
]]
-- Let's killall ffmpeg instance first after awesome (re)-starts if there's any
awful.spawn.easy_async_with_shell(killall_ffmpeg, function(stdout) end, false)


local create_save_directory = function()

	local create_dir_cmd = [[
	dir=]] .. record_directory .. [[

	if [ ! -d $dir ]; then
		mkdir -p $dir
	fi
	]]

	-- Create dir if not exists
	awful.spawn.easy_async_with_shell(create_dir_cmd, function(stdout) end, false)

end

local return_ffmpeg_cmd = function()
	local record_without_audio_cmd = [[
	ffmpeg -video_size ]] .. record_resolution .. [[ -framerate ]] .. record_fps .. [[ -f x11grab \
	-i :0.0+0,0 ]] .. record_directory ..[[`date '+%Y-%m-%d_%H-%M-%S'`.mp4
	]]

	return record_without_audio_cmd
end


local return_ffmpeg_alsa_cmd = function()
	local record_with_alsa_cmd = [[
	ffmpeg -video_size ]] .. record_resolution .. [[ -framerate ]] .. record_fps .. [[ -f x11grab \
	-i :0.0+0,0 -f alsa -ac 2 -i hw:0 ]] .. record_directory ..[[`date '+%Y-%m-%d_%H-%M-%S'`.mp4
	]]

	return record_with_alsa_cmd
end


local return_ffmpeg_pulse_cmd = function()
	local record_with_pulse_cmd = [[
	ffmpeg -video_size ]] .. record_resolution .. [[ -framerate ]] .. record_fps .. [[ -f x11grab \
	-i :0.0+0,0 -f pulse -ac 2 -i default ]] .. record_directory ..[[`date '+%Y-%m-%d_%H-%M-%S'`.mp4
	]]

	return record_with_pulse_cmd
end



-- Start recording
local recording_start = function()

	local ffmpeg_command = ''

	-- If audio is on
	if status_audio then

		-- Turn on Microphone command
		local turn_on_microphone = [[
		amixer set Capture cap
		amixer set Capture ]].. record_mic_power ..[[%
		]]
		-- Turn on mic
		awful.spawn.easy_async_with_shell(turn_on_microphone, function(stdout) end, false)

		-- Alsa mode
		if record_input_device == 'alsa' then

			ffmpeg_command = return_ffmpeg_alsa_cmd()
			ffmpeg_pid = awful.spawn.easy_async_with_shell(ffmpeg_command, function(stdout, stderr) 
				-- require('naughty').notification({message=stderr})
			end, false)

		-- Pulse
		elseif record_input_device == 'pulse' then

			ffmpeg_command = return_ffmpeg_pulse_cmd()
			ffmpeg_pid = awful.spawn.easy_async_with_shell(ffmpeg_command, function(stdout, stderr) 
				-- require('naughty').notification({message=stderr})
			end, false)

		end
 
	else
 
		-- Record without audio
		ffmpeg_command = return_ffmpeg_cmd()
		ffmpeg_pid = awful.spawn.easy_async_with_shell(ffmpeg_command, function(stdout, stderr) 
			-- require('naughty').notification({message=stderr})
		end, false)


	end

end

-- Stop recording
local recording_stop = function()
	-- Let's kill those bastarts
	awesome.kill(ffmpeg_pid, awesome.unix_signal.SIGINT)

end


-- Accepts: 'start_countdown', 'start_recording', 'stop_timer', 'stop_recording'
awesome.connect_signal('module::screen_recorder', function(mode) 

	-- Start counting down
	if mode == 'start_countdown' then
		-- Change UI Value
		recording_toggle.toggle_text:set_text('Hold on')

		-- Start counting down
		counter_timer()

	-- Start screen recording
	elseif mode == 'start_recording' then

		-- Check or Create directory
		create_save_directory()

		-- Start recording
		recording_start()

		-- Change UI Values
		recording_toggle.toggle_text:set_text('Stop Recording')
		counter_text.opacity = 0.0
		recording_status_imagebox.icon:set_image(PATH_TO_ICONS .. 'recording' .. '.svg')
		recorder_button_imagebox.button_icon:set_image(PATH_TO_ICONS .. 'recorder-on' .. '.svg')

		-- Change recording status to true
		status_recording = true

		-- Hide screen recorder screen
		for s in screen do
			s.recorder_screen.visible = false
		end


	-- Stop counting down
	elseif mode == 'stop_timer' then

		-- Reset UI values
		recording_toggle.toggle_text:set_text('Start Recording')
		counter_text.opacity = 0.0
		counter_text:set_text('4')

		-- Set countdown status to false
		status_countdown = false

		-- Stop timer
		countdown_timer:stop()  

	elseif mode == 'stop_recording' then
		-- Stop recording
		recording_stop()

		recording_toggle.toggle_text:set_text('Start Recording')
		counter_text.opacity = 0.0
		counter_text:set_text('4')
		recording_status_imagebox.icon:set_image(PATH_TO_ICONS .. 'start-recording' .. '.svg')
		recorder_button_imagebox.button_icon:set_image(PATH_TO_ICONS .. 'recorder-off' .. '.svg')

		-- Set statuses to false
		status_countdown = false
		status_recording = false
	end
end)



local recording_button = clickable_container(wibox.container.margin(recording_toggle, dpi(7), dpi(7), dpi(7), dpi(7)))
recording_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()

				if not status_recording and not status_countdown then
					-- Start countdown timer
					awesome.emit_signal('module::screen_recorder', 'start_countdown')
				
				elseif not status_recording and status_countdown then
					-- Stop countdown timer
					awesome.emit_signal('module::screen_recorder', 'stop_timer')
				
				else
					-- Stop Recording
					awesome.emit_signal('module::screen_recorder', 'stop_recording')
				end
			end
		)
	)
)


local recording_audio_imagebox = wibox.widget {
	{
		id = 'icon',
		forced_height = dpi(30),
		image = PATH_TO_ICONS .. 'audio' .. '.svg',
		resize = true,
		widget = wibox.widget.imagebox
	},
	margins = dpi(10),
	widget = wibox.container.margin
}


local audio_button = clickable_container(
	wibox.container.margin(
		recording_audio_imagebox, dpi(7), dpi(7), dpi(7), dpi(7)
	)
)

local close_imagebox = wibox.widget {
	{
		id = 'icon',
		forced_height = dpi(30),
		forced_width = dpi(30),
		image = PATH_TO_ICONS .. 'close-screen' .. '.svg',
		resize = true,
		widget = wibox.widget.imagebox
	},
	margins = dpi(5),
	widget = wibox.container.margin
}



local close_button = clickable_container(
	wibox.container.margin(
		close_imagebox, dpi(7), dpi(7), dpi(7), dpi(7)
	)
)

close_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				for s in screen do
					s.recorder_screen.visible = false
				end
			end
		)
	)
)


local audio_status = wibox.widget {
	markup = 'Microphone Audio \n<b>Off</b>',
	font = 'SFNS Display Regular 12',
	align = 'center',
	valign = 'center',
	widget = wibox.widget.textbox
}


local record_audio_widget = wibox.widget {
	{
		layout = wibox.layout.align.horizontal,
		expand ='none',
		nil,
		audio_button,    
		nil
	},
	bg = beautiful.groups_bg,
	shape = gears.shape.circle,
	widget = wibox.container.background
}


local record_audio = function()
	if not status_audio then
		-- Update GUI
		record_audio_widget.bg = '#EE4F84' .. 'AA'
		audio_status.markup = 'Microphone Audio \n<b>On</b>'

		-- Change status
		status_audio = true

	else
		-- Update GUI
		record_audio_widget.bg = beautiful.groups_bg
		audio_status.markup = 'Microphone Audio \n<b>Off</b>'

		-- Change status
		status_audio = false
	end
end


audio_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				if not status_recording then
					record_audio()
				end
			end
		)
	)
)


local recording_body = wibox.widget {
	layout = wibox.layout.fixed.vertical,
	spacing = dpi(20),
	counter_text,
	{
		{
			layout = wibox.layout.fixed.vertical,
			spacing = dpi(0),
			{
				{
					layout = wibox.layout.fixed.vertical,
					spacing = dpi(10),
					recording_status_imagebox,
					recording_header
				},
				margins = dpi(20),
				widget = wibox.layout.margin
			},
			separator,
			recording_button
		},
		bg = beautiful.groups_bg,
		shape = function(cr, width, height)
			gears.shape.rounded_rect(cr, width, height, dpi(42)) end,
		widget = wibox.container.background
	},
	{
		record_audio_widget,
		audio_status,
		spacing = dpi(10),
		layout = wibox.layout.fixed.vertical
	}
}

-- Create screen_recorder on every screen
screen.connect_signal("request::desktop_decoration", function(s)

	s.recorder_screen = wibox
	({
		ontop = true,
		screen = s,
		type = 'dock',
		height = s.geometry.height,
		width = s.geometry.width,
		x = s.geometry.x,
		y = s.geometry.y,
		bg = beautiful.background,
		fg = beautiful.fg_normal
	})

	s.recorder_screen : setup {
		layout = wibox.layout.align.vertical,
		expand = 'none',
		nil,
		{
			layout = wibox.layout.align.horizontal,
			expand = 'none',
			nil,
			recording_body,
			nil,
		},
		{
			layout = wibox.layout.align.horizontal,
			expand = 'none',
			nil,
			{
				close_button,
				bg = beautiful.groups_bg,
				shape = function(cr, width, height)
				 gears.shape.partially_rounded_rect(cr, width, height, true, true, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			nil
		},
	}

	-- Toggle screen_recorder
	-- Should've been named open_recorder 
	-- But, oh well...
	toggle_recorder = function()

		for s in screen do
			s.recorder_screen.visible = false
		end

		awful.screen.focused().recorder_screen.visible = not awful.screen.focused().recorder_screen.visible
	end

	-- Hide on right-click
	s.recorder_screen:buttons(
		gears.table.join(
			awful.button(
				{},
				3,
				nil,
				function()
					-- Hide all instance of screen_recorder
					for s in screen do
						s.recorder_screen.visible = false
					end
				end
			)
		)
	)

end)

record_tbl.toggle_recorder = toggle_recorder
record_tbl.recorder_button_imagebox = recorder_button_imagebox
record_tbl.config_getter = config_getter

return record_tbl