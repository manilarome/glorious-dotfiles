local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local naughty = require('naughty')
local watch = require('awful.widget.watch')

local clickable_container = require('widget.material.clickable-container')
local dpi = require('beautiful').xresources.apply_dpi

local HOME = os.getenv('HOME')
local PATH_TO_ICONS = HOME .. '/.config/awesome/widget/screen-recorder/icons/'


local recorder_table = require('widget.screen-recorder.recorder-screen')

local toggle_recorder = recorder_table.toggle_recorder
local recorder_button_imagebox = recorder_table.recorder_button_imagebox

local record_resolution = recorder_table.config_getter



--   #####                                
--  #     #  ####  #    # ###### #  ####  
--  #       #    # ##   # #      # #    # 
--  #       #    # # #  # #####  # #      
--  #       #    # #  # # #      # #  ### 
--  #     # #    # #   ## #      # #    # 
--   #####   ####  #    # #      #  ####  

-- Change your configuration here
--    res: the resolution of your monitor
--        example: 800x600, 1024x768. 1280x720, etc
--    fps: the fps
--        example: 25, 30, 60, etc
--    dir: the place where we will save the recording
--        example: HOME .. '/' .. Recordings/
--            Note: The widget automatically creates a directory if it does not exist
--    dev: input device that we will use
--        example: alsa or pulse
--            Note: Sometimes alsa returns an error so it's recommended to use pulse
--    mic_power: microphone power, can be set to 0 - 100
--        example 10

local res       = '1366x768'
local fps       = '30'
local dir       = HOME .. '/' .. 'Videos/Recordings/'
local dev       = 'pulse'
local mic_power = '10' 


--        #  #####                                
--       #  #     #  ####  #    # ###### #  ####  
--      #   #       #    # ##   # #      # #    # 
--     #    #       #    # # #  # #####  # #      
--    #     #       #    # #  # # #      # #  ### 
--   #      #     # #    # #   ## #      # #    # 
--  #        #####   ####  #    # #      #  ####  


-- Return value to rocorder-screen's table
record_resolution(res, fps, dir, dev, mic_power)

local return_button = function()

	local recorder_button = clickable_container(wibox.container.margin(recorder_button_imagebox, dpi(8), dpi(8), dpi(8), dpi(8)))
	recorder_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					-- Toggle recorder screen
					toggle_recorder()
				end
			)
		)
	)

	return recorder_button

end

return return_button