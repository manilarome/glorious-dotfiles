local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local mat_list_item = require('widget.material.list-item')
local mat_list_sep = require('widget.material.list-item-separator')

local barColor = beautiful.groups_bg

local hardware_header = wibox.widget
{
	text = 'Hardware Monitor',
	font = 'SFNS Display 12',
	align = 'center',
	widget = wibox.widget.textbox

}

return wibox.widget {
	layout = wibox.layout.fixed.vertical,
	{
		{
			hardware_header,
			bg = beautiful.groups_title_bg,
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(cr, width, height, true, true, false, false, beautiful.groups_radius) end,
			widget = wibox.container.background,
		},
		widget = mat_list_item,
	},
	{
		{
			require('widget.cpu.cpu-meter'),
			bg = barColor,
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
			widget = wibox.container.background
		},
		widget = mat_list_item,
	},
	{
		{
			require('widget.ram.ram-meter'),
			bg = barColor,
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
			widget = wibox.container.background
		},
		widget = mat_list_item,
	},
	layout = wibox.layout.fixed.vertical,
	{
		{
			require('widget.temperature.temperature-meter'),
			bg = barColor,
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
			widget = wibox.container.background
		},
		widget = mat_list_item,
	},
	{
		{
			require('widget.harddrive.harddrive-meter'),
			bg = barColor,
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(cr, width, height, false, false, true, true, beautiful.groups_radius) end,
			widget = wibox.container.background
		},
		widget = mat_list_item,
	},
}
