local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')

local dpi = require('beautiful').xresources.apply_dpi
local mat_list_item = require('widget.material.list-item')
local mat_list_sep = require('widget.material.list-item-separator')

local barColor = beautiful.groups_bg

local quick_header = wibox.widget {
	text = 'Quick Settings',
	font = 'SFNS Display 12',
	align = 'center',
	widget = wibox.widget.textbox
}

return wibox.widget {
	layout = wibox.layout.fixed.vertical,
	spacing = dpi(7),
	{
		layout = wibox.layout.fixed.vertical,
		{
			{
				quick_header,
				bg = beautiful.groups_title_bg,
				shape = function(cr, width, height)
					gears.shape.partially_rounded_rect(cr, width, height, true, true, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background,
			},
			widget = mat_list_item,
		},
		-- Brightness slider
		{
			{
				require('widget.brightness.brightness-slider'),
				bg = barColor,
				shape = function(cr, width, height)
					gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item,
		},
		{
			{
				require('widget.volume.volume-slider'),
				bg = barColor,
				shape = function(cr, width, height)
					gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item
		},

		{
			{
				require('widget.action-center.wifi-action'),
				bg = barColor,
				shape = function(cr, width, height)
					gears.shape.partially_rounded_rect(cr, width, height, false, false, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item,
		},

		-- Bluetooth Connection
		{
			{
				require('widget.action-center.bluetooth-action'),
				bg = barColor,
				shape = function(cr, width, height)
					 gears.shape.partially_rounded_rect(cr, width, height, false, false, true, true, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item,
		},
	},
	{
		layout = wibox.layout.fixed.vertical,
		-- Window FX Toggle
		{
			{
				require('widget.window-effects.toggle-blur-action'),
				bg = barColor,
				shape = function(cr, width, height)
					 gears.shape.partially_rounded_rect(cr, width, height, true, true, false, false, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item,
		},
		-- Blur strength Slider  
		{
			{
				require('widget.window-effects.blur-strength-action'),
				bg = barColor,
				shape = function(cr, width, height)
					gears.shape.partially_rounded_rect(cr, width, height, false, false, true, true, beautiful.groups_radius) end,
				widget = wibox.container.background
			},
			widget = mat_list_item,
		}
	}
}
