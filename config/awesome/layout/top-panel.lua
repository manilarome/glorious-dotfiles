local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

local clickable_container = require('widget.material.clickable-container')
local mat_icon_button = require('widget.material.icon-button')
local mat_icon = require('widget.material.icon')

local TaskList = require('widget.task-list')

local icons = require('theme.icons')
local dpi = require('beautiful').xresources.apply_dpi

-- Toggles tray if systemtray widget is not used
awesome.connect_signal("toggle_tray", function()
	if screen.primary.systray then
		-- The global var used below is set in systemtray widget
		if _G.systemtray_button then
			return
		end
		screen.primary.systray.visible = not screen.primary.systray.visible
	end
end)


-- Top Panel
local TopPanel = function(s, offset)

	local offsetx = 0
	if offset == true then
		offsetx = dpi(45)
	end

	local panel = wibox
	{
		ontop = true,
		screen = s,
		type = 'dock',
		height = dpi(30),
		width = s.geometry.width - offsetx,
		x = s.geometry.x + offsetx,
		y = s.geometry.y,
		stretch = false,
		bg = beautiful.background,
		fg = beautiful.fg_normal
	}
	

	panel:struts
	{
		top = dpi(30)
	}

	-- The `+` sign in top panel
	s.add_button = mat_icon_button(mat_icon(icons.plus, dpi(16))) -- add button -- 24
	s.add_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					awful.spawn(
						awful.screen.focused().selected_tag.defaultApp,
						{
							tag = _G.mouse.screen.selected_tag,
							placement = awful.placement.bottom_right
						}
					)
				end
			)
		)
	)


	-- Create an imagebox widget which will contains an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	local LayoutBox = function(s)
		local layoutBox = clickable_container(awful.widget.layoutbox(s))
		layoutBox:buttons(
			awful.util.table.join(
				awful.button(
					{},
					1,
					function()
						awful.layout.inc(1)
					end
				),
				awful.button(
					{},
					3,
					function()
						awful.layout.inc(-1)
					end
				),
				awful.button(
					{},
					4,
					function()
						awful.layout.inc(1)
					end
				),
				awful.button(
					{},
					5,
					function()
						awful.layout.inc(-1)
					end
				)
			)
		)
		return layoutBox
	end

	-- Clock Widget
	-- Get Time/Date format using `man strftime`
	s.clock_widget = wibox.widget.textclock(
		'<span font="SFNS Display Bold 10">%l:%M %p</span>',
		1
	)

	-- Have a responsive cursor if hovers on clock widget
	s.clock_widget:connect_signal(
		'mouse::enter',
		function()
			-- Hm, no idea how to get the wibox from this signal's arguments...
			local w = _G.mouse.current_wibox
			if w then
				old_cursor, old_wibox = w.cursor, w
				w.cursor = 'hand1'
			end
		end
	)
	-- Have a responsive cursor when it leaves the clock widget
	s.clock_widget:connect_signal(
		'mouse::leave',
		function()
			if old_wibox then
				old_wibox.cursor = old_cursor
				old_wibox = nil
			end
		end
	)

	-- Clock tooltip
	s.clock_tooltip = awful.tooltip
	{
		objects = {s.clock_widget},
		mode = 'outside',
		align = 'right',
		delay_show = 1,
		markup = os.date("Today is <b>%B %d, %Y</b>.\nAnd it's fucking <b>%A</b>!"),
		preferred_positions = {'right', 'left', 'top', 'bottom'},
		margin_leftright = dpi(8),
		margin_topbottom = dpi(8)
	}


	s.clock_widget:connect_signal('button::press', function() 
		-- Hide the tooltip when you press the clock widget
		if s.clock_tooltip.visible then
			s.clock_tooltip.visible = false
		end
	end)


	-- Calendar popup
	s.month_calendar    = awful.widget.calendar_popup.month({
		start_sunday      = true,
		spacing           = dpi(5),
		font              = 'SFNS Display Regular 10',
		long_weekdays     = true,
		margin            = dpi(5),
		screen            = s,
		style_month       = { 
			border_width    = dpi(0), 
			padding         = dpi(20),
			shape           = function(cr, width, height)
				gears.shape.partially_rounded_rect(
					cr, width, height, true, true, true, true, beautiful.groups_radius)
			end
		},  
		style_header      = { 
			border_width    = 0, 
			bg_color        = beautiful.transparent
		},
		style_weekday     = { 
			border_width    = 0, 
			bg_color        = beautiful.transparent
		},

		style_normal      = { 
			border_width    = 0, 
			bg_color        = beautiful.transparent
		},
		style_focus       = { 
			border_width    = dpi(0), 
			border_color    = beautiful.fg_normal, 
			bg_color        = beautiful.accent, 
			shape           = function(cr, width, height)
				gears.shape.partially_rounded_rect(
					cr, width, height, true, true, true, true, dpi(4))
			end,
		},
	})

	s.month_calendar:attach(s.clock_widget, "tc" , { on_pressed = true, on_hover = false })

	-- System tray
	s.systray = wibox.widget {
		visible = false,
		base_size = 20,
		horizontal = true,
		screen = 'primary',
		widget = wibox.widget.systray
	}


	-- Instantiate widgets
	s.system_tray = awful.widget.only_on_screen(require('widget.systemtray'), 'primary')
	s.updater     = require('widget.package-updater')()
	s.screen_rec  = require('widget.screen-recorder')()
	s.music       = require('widget.music')()
	s.bluetooth   = require('widget.bluetooth')()
	s.wifi        = require('widget.wifi')()
	s.battery     = require('widget.battery')()
	s.search      = require('widget.search')()
	s.r_dashboard = require('widget.right-dashboard')()


	-- Top panel setup
	panel : setup {
		layout = wibox.layout.align.horizontal,
		expand = "none",
		{
			layout = wibox.layout.fixed.horizontal,
			TaskList(s),
			s.add_button
		},
		s.clock_widget,
		{
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(5),
			{
				s.systray,
				margins = dpi(5),
				widget = wibox.container.margin
			},
			s.system_tray,
			s.updater,
			s.screen_rec,
			s.music,
			s.bluetooth,
			s.wifi,
			s.battery,
			s.search,
			LayoutBox(s),
			s.r_dashboard
		}
	}

	return panel
end


return TopPanel
